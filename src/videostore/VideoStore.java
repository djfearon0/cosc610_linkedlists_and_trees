/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videostore;

import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author frost
 */
public class VideoStore {

    private static LinkedList queue = new LinkedList();
    private static int numVideos;
    private static int numCustomers;
    private static int numTransactions;
    private static boolean print = false;

    private static SLL sllC = null;
    private static DList dllC = null;
    private static BinarySearchTree bstC = null;
    private static AVLTree avlC = null;

    private static SLL sllV = null;
    private static DList dllV = null;
    private static BinarySearchTree bstV = null;
    private static AVLTree avlV = null;

    private static String dataStructure = "";

    private static Scanner input = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean isValid = false;

        if (!(args.length >= 1)) {
            System.out.println("Please specify a data structure (\"SLL\", \"DLL\", \"BST\" or \"AVL\") as a parameter when executing.");
            System.exit(0);
        } else if (args.length == 1) {
            dataStructure = args[0];
            isValid = validDataStructure();
            if (!isValid) {
                System.out.println("Please specify a data structure as a parameter when executing.");
                System.exit(0);
            } else {
                switch (dataStructure) {
                    case "SLL":
                        sllC = new SLL();
                        sllV = new SLL();
                        break;
                    case "DLL":
                        dllC = new DList();
                        dllV = new DList();
                        break;
                    case "BST":
                        bstC = new BinarySearchTree();
                        bstV = new BinarySearchTree();
                        break;
                    case "AVL":
                        avlC = new AVLTree();
                        avlV = new AVLTree();
                        break;
                }
                print = true;
            }
        } else if (args.length == 4) {
            dataStructure = args[0];
            isValid = validDataStructure();
            if (!isValid) {
                System.out.println("Please specify a data structure as a parameter when executing.");
                System.exit(0);
            } else {
                switch (dataStructure) {
                    case "SLL":
                        sllC = new SLL();
                        sllV = new SLL();
                        break;
                    case "DLL":
                        dllC = new DList();
                        dllV = new DList();
                        break;
                    case "BST":
                        bstC = new BinarySearchTree();
                        bstV = new BinarySearchTree();
                        break;
                    case "AVL":
                        avlC = new AVLTree();
                        avlV = new AVLTree();
                        break;
                }

                numVideos = Integer.parseInt(args[1]);
                numCustomers = Integer.parseInt(args[2]);
                numTransactions = Integer.parseInt(args[3]);

                generateTestData();
                long start = System.nanoTime();
                runSim();
                long runTime = System.nanoTime() - start;
                long runTimeMS = TimeUnit.MILLISECONDS.convert(runTime, TimeUnit.NANOSECONDS);
                long runTimeS = TimeUnit.SECONDS.convert(runTime, TimeUnit.NANOSECONDS);
                System.out.printf("The process with %s took:\n"
                        + "Time in Nanoseconds: %d\n"
                        + "Time in Milliseconds: %d\n"
                        + "Time in Seconds: %d\n", dataStructure, runTime, runTimeMS, runTimeS);
                System.exit(0);
            }
        } else {
            System.out.println("Unexpected parameters encountered.");
            System.exit(0);
        }

        String c;

        while (true) {
            printMenu();
            c = input.nextLine();
            runCommand(Integer.parseInt(c));
        }
    }

    private static void runSim() {
        while (!queue.isEmpty()) {
            Transaction t = (Transaction) queue.removeFirst();
            runSimCommand(t.getCustomer(), t.getVideo(), t.getAction());
        }
    }

    private static void runSimCommand(Customer c, Video v, int a) {
        boolean in = false;
        switch (a) {
            case 5:
                switch (dataStructure) {
                    case "SLL":
                        in = (!sllV.contains(v, "v").equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                    case "DLL":
                        in = (!dllV.contains(v, "v").equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                    case "BST":
                        in = (!bstV.search(v).equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                    case "AVL":
                        in = (!avlV.search(v).equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                }
                break;
            case 6:
                switch (dataStructure) {
                    case "SLL":
                        int stuff = sllV.indexOf((v), "v");
                        Node sTemp = sllV.removeAt(stuff);
                        Customer curr = (Customer) sllC.removeAt(sllC.indexOf(c, "c")).getElement();
                        if (sTemp != null) {
                            Video svTemp = ((Video) (sTemp.getElement()));
                            curr.setRentedVideo(svTemp);
                            sllC.add(new Node(curr, null));
                        }
                        break;
                    case "DLL":
                        if (!dllV.contains(v, "v").equals(-1)) {
                            Video dvTemp = (Video) (dllV.remove(new DNode(v, null, null), "v").getElement());
                            Customer currD = (Customer) dllC.remove(new DNode(c, null, null), "c").getElement();
                            currD.setRentedVideo(dvTemp);
                            dllC.addLast(new DNode(currD, null, null));
                        }
                        break;
                    case "BST":
                        if (bstV.search(v) != null) {
                            BinaryTreeNode bTemp = bstV.remove(v);
                            BinaryTreeNode cNode = bstC.remove(c);
                            if (cNode != null) {
                                Customer currBST = (Customer) cNode.data;
                                if (bTemp != null) {
                                    Video bstvTemp = (Video) (bTemp.data);
                                    currBST.rentedVideosBST.insert(bstvTemp);
                                    bstC.insert(currBST);
                                }
                            }
                        }
                        break;
                    case "AVL":
                        if (avlV.search(v) != null) {
                            BinaryTreeNode aTemp = avlV.remove(v);
                            BinaryTreeNode acNode = avlC.remove(c);
                            if (acNode != null) {
                                Customer currAVL = (Customer) acNode.data;
                                if (aTemp != null) {
                                    Video avlvTemp = (Video) (aTemp.data);
                                    currAVL.rentedVideosAVL.insert(avlvTemp);
                                    avlC.insert(currAVL);
                                }
                            }
                        }
                        break;
                }
                break;
            case 7:
                switch (dataStructure) {
                    case "SLL":
                        Node sTemp = c.rentedVideosSLL.removeAt(c.rentedVideosSLL.indexOf((v), "v"));
                        if (sTemp != null) {
                            sllV.add(sTemp);
                        }
                        break;
                    case "DLL":
                        DNode dvTemp = (c.rentedVideosDLL.remove(new DNode(v, null, null), "v"));
                        if (dvTemp != null) {
                            Video dllVideo = (Video) dvTemp.getElement();
                            c.setRentedVideo(dllVideo);
                        }
                        break;
                    case "BST":
                        if (c.rentedVideosBST.search(v) != null) {
                            BinaryTreeNode bTemp = c.rentedVideosBST.remove(v);
                            if (bTemp != null) {
                                Video bstvTemp = (Video) (bTemp.data);
                                bstV.insert(bstvTemp);
                            }
                        }
                        break;
                    case "AVL":
                        if (c.rentedVideosAVL.search(v) != null) {
                            BinaryTreeNode aTemp = c.rentedVideosAVL.remove(v);
                            if (aTemp != null) {
                                Video avlvTemp = (Video) (aTemp.data);
                                avlV.insert(avlvTemp);
                            }
                        }
                        break;
                }
                break;
        }
    }

    private static boolean validDataStructure() {

        String[] validDS = {"SLL", "DLL", "BST", "AVL"};

        for (String dsType : validDS) {
            if (dsType.equals(dataStructure)) {
                return true;
            }
        }
        return false;
    }

    private static void printMenu() {
        System.out.println("====================================================\n\n"
                + "Please select one of the following:\n"
                + "1: To add a video\n"
                + "2: To delete a video\n"
                + "3: To add a customer\n"
                + "4: To delete a customer\n"
                + "5: To check if a particular video is in the store\n"
                + "6: To check out a video\n"
                + "7: To check in a video\n"
                + "8: To print all customers\n"
                + "9: To print all videos\n"
                + "10: To print in store videos\n"
                + "11: To print all rented videos\n"
                + "12: To print the videos rented by a customer\n"
                + "13: To exit\n\n"
                + "====================================================");
    }

    private static void runCommand(int command) {
        String t;
        String i;
        Customer c;
        Video v;
        switch (command) {
            case 1:
                System.out.print("Please enter the title of the new video: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the new video: ");
                i = input.nextLine();
                System.out.println();
                v = new Video(t, i);
                //Add logic
                switch (dataStructure) {
                    case "SLL":
                        sllV.add(new Node(v, null));
                        break;
                    case "DLL":
                        dllV.addLast(new DNode(v, null, null));
                        break;
                    case "BST":
                        bstV.insert(v);
                        break;
                    case "AVL":
                        avlV.insert(v);
                        break;
                }
                break;
            case 2:
                System.out.print("Please enter the title of the video: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the video: ");
                i = input.nextLine();
                System.out.println();
                v = new Video(t, i);
                //Delete logic
                switch (dataStructure) {
                    case "SLL":
                        sllV.removeAt(sllV.indexOf(v, "v"));
                        break;
                    case "DLL":
                        dllV.remove(new DNode(v, null, null), "v");
                        break;
                    case "BST":
                        bstV.remove(v);
                        break;
                    case "AVL":
                        avlV.remove(v);
                        break;
                }
                break;
            case 3:
                System.out.print("Please enter the name of the new customer: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the new customer: ");
                i = input.nextLine();
                System.out.println();
                c = new Customer(t, i, dataStructure);
                //Add logic
                switch (dataStructure) {
                    case "SLL":
                        sllC.add(new Node(c, null));
                        break;
                    case "DLL":
                        dllC.addLast(new DNode(c, null, null));
                        break;
                    case "BST":
                        bstC.insert(c);
                        break;
                    case "AVL":
                        avlC.insert(c);
                        break;
                }
                break;
            case 4:
                System.out.print("Please enter the name of the customer: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the customer: ");
                i = input.nextLine();
                System.out.println();
                c = new Customer(t, i, dataStructure);
                //Delete logic
                switch (dataStructure) {
                    case "SLL":
                        sllC.removeAt(sllC.indexOf(c, "c"));
                        break;
                    case "DLL":
                        dllC.remove(new DNode(c, null, null), "c");
                        break;
                    case "BST":
                        bstC.remove(c);
                        break;
                    case "AVL":
                        avlC.remove(c);
                        break;
                }
                break;
            case 5:
                System.out.print("Please enter the title of the video: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the video: ");
                i = input.nextLine();
                System.out.println();
                v = new Video(t, i);
                boolean in = false;
                //Search logic
                switch (dataStructure) {
                    case "SLL":
                        in = (!sllV.contains(v, "v").equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                    case "DLL":
                        in = (!dllV.contains(v, "v").equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                    case "BST":
                        in = (!bstV.search(v).equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                    case "AVL":
                        in = (!avlV.search(v).equals(-1) ? true : false);
                        if (print) {
                            System.out.println(v.getTitle() + " is in: " + in);
                        }
                        break;
                }
                break;
            case 6:
                System.out.print("Please enter the name of the customer: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the customer: ");
                i = input.nextLine();
                System.out.println();
                c = new Customer(t, i, dataStructure);
                System.out.print("Please enter the title of the video: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the video: ");
                i = input.nextLine();
                System.out.println();
                v = new Video(t, i);
                //Check out logic
                switch (dataStructure) {
                    case "SLL":
                        int stuff = sllV.indexOf((v), "v");
                        Node sTemp = sllV.removeAt(stuff);
                        Customer curr = (Customer) sllC.removeAt(sllC.indexOf(c, "c")).getElement();
                        if (sTemp != null) {
                            Video svTemp = ((Video) (sTemp.getElement()));
                            curr.setRentedVideo(svTemp);
                            sllC.add(new Node(curr, null));
                        }
                        break;
                    case "DLL":
                        Video dvTemp = (Video) (dllV.remove(new DNode(v, null, null), "v").getElement());
                        Customer currD = (Customer) dllC.remove(new DNode(c, null, null), "c").getElement();
                        currD.setRentedVideo(dvTemp);
                        dllC.addLast(new DNode(currD, null, null));
                        break;
                    case "BST":
                        if (bstV.search(v) != null) {
                            BinaryTreeNode bTemp = bstV.remove(v);
                            Customer currBST = (Customer) bstC.remove(c).data;
                            if (bTemp != null) {
                                Video bstvTemp = (Video) (bTemp.data);
                                currBST.rentedVideosBST.insert(bstvTemp);
                            }
                        }
                        break;
                    case "AVL":
                        if (avlV.search(v) != null) {
                            BinaryTreeNode aTemp = avlV.remove(v);
                            Customer currAVL = (Customer) bstC.remove(c).data;
                            if (aTemp != null) {
                                Video avlvTemp = (Video) (aTemp.data);
                                currAVL.rentedVideosAVL.insert(avlvTemp);
                            }
                        }
                        break;
                }
                break;
            case 7:
                System.out.print("Please enter the name of the customer: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the customer: ");
                i = input.nextLine();
                System.out.println();
                c = new Customer(t, i, dataStructure);
                System.out.print("Please enter the title of the video: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the video: ");
                i = input.nextLine();
                System.out.println();
                v = new Video(t, i);
                //Check in logic
                switch (dataStructure) {
                    case "SLL":
                        Node sTemp = c.rentedVideosSLL.removeAt(c.rentedVideosSLL.indexOf((v), "v"));
                        if (sTemp != null) {
                            sllV.add(sTemp);
                        }
                        break;
                    case "DLL":
                        DNode dvTemp = (c.rentedVideosDLL.remove(new DNode(v, null, null), "v"));
                        if (dvTemp != null) {
                            Video dllVideo = (Video) dvTemp.getElement();
                            c.setRentedVideo(dllVideo);
                        }
                        break;
                    case "BST":
                        if (c.rentedVideosBST.search(v) != null) {
                            BinaryTreeNode bTemp = c.rentedVideosBST.remove(v);
                            if (bTemp != null) {
                                Video bstvTemp = (Video) (bTemp.data);
                                bstV.insert(bstvTemp);
                            }
                        }
                        break;
                    case "AVL":
                        if (c.rentedVideosAVL.search(v) != null) {
                            BinaryTreeNode aTemp = c.rentedVideosAVL.remove(v);
                            if (aTemp != null) {
                                Video avlvTemp = (Video) (aTemp.data);
                                avlV.insert(avlvTemp);
                            }
                        }
                        break;
                }
                break;
            case 8:
                //Print customers
                switch (dataStructure) {
                    case "SLL":
                        sllC.print();
                        break;
                    case "DLL":
                        dllC.print();
                        break;
                    case "BST":
                        bstC.inorderTraversal();
                        break;
                    case "AVL":
                        avlC.inorderTraversal();
                        break;
                }
                break;
            case 9:
                //Print all videos
                runCommand(10);
                runCommand(11);
                break;
            case 10:
                //Print videos in store
                switch (dataStructure) {
                    case "SLL":
                        sllV.print();
                        break;
                    case "DLL":
                        dllV.print();
                        break;
                    case "BST":
                        bstV.inorderTraversal();
                        break;
                    case "AVL":
                        avlV.inorderTraversal();
                        break;
                }
                break;
            case 11:
                //Print all rented videos
                Object[] arr;
                switch (dataStructure) {
                    case "SLL":
                        arr = sllC.toArray();
                        if (arr != null) {
                            for (Object o : arr) {
                                System.out.print(((Customer) o).getName() + ": ");
                                ((Customer) o).getRentedVideos();
                            }
                        }
                        break;
                    case "DLL":
                        arr = dllC.toArray();
                        if (arr != null) {
                            for (Object o : arr) {
                                System.out.print(((Customer) o).getName() + ": ");
                                ((Customer) o).getRentedVideos();
                            }
                        }
                        break;
                    case "BST":
                        arr = bstC.toArray();
                        if (arr != null) {
                            for (Object o : arr) {
                                System.out.print(((Customer) o).getName() + ": ");
                                ((Customer) o).getRentedVideos();
                            }
                        }
                        break;
                    case "AVL":
                        arr = avlC.toArray();
                        if (arr != null) {
                            for (Object o : arr) {
                                System.out.print(((Customer) o).getName() + ": ");
                                ((Customer) o).getRentedVideos();
                            }
                        }
                        break;
                }
                break;
            case 12:
                System.out.print("Please enter the name of the customer: ");
                t = input.nextLine();
                System.out.println();
                System.out.print("Please enter the id of the customer: ");
                i = input.nextLine();
                System.out.println();
                c = new Customer(t, i, dataStructure);
                //Print rented videos logic
                c.getRentedVideos();
                break;
            case 13:
                System.out.println("Thank you for using the video store system!");
                System.exit(0);
                break;
            default:
                System.out.println("Please enter a valid number from the list below.");
                break;
        }
    }

    private static void generateTestData() {

        int[] actions = {5, 6, 7};

        Random rand = new Random();

        for (int i = 0; i < numVideos; i++) {
            Video v = new Video("v" + i, "" + i);
            switch (dataStructure) {
                case "SLL":
                    sllV.add(new Node(v, null));
                    break;
                case "DLL":
                    dllV.addLast(new DNode(v, null, null));
                    break;
                case "BST":
                    bstV.insert(v);
                    break;
                case "AVL":
                    avlV.insert(v);
                    break;
            }
        }
        for (int i = 0; i < numCustomers; i++) {
            Customer c = new Customer("c" + i, "" + i, dataStructure);
            switch (dataStructure) {
                case "SLL":
                    sllC.add(new Node(c, null));
                    break;
                case "DLL":
                    dllC.addLast(new DNode(c, null, null));
                    break;
                case "BST":
                    bstC.insert(c);
                    break;
                case "AVL":
                    avlC.insert(c);
                    break;
            }
        }
        for (int i = 0; i < numTransactions; i++) {
            int cId = rand.nextInt(numCustomers);
            int vId = rand.nextInt(numVideos);
            int choice = rand.nextInt(3);
            Transaction t = new Transaction(new Customer("c" + cId, "" + cId, dataStructure), new Video("v" + vId, "" + vId), actions[choice]);
            queue.addLast(t);
        }
    }

}

class Video implements Comparable {

    private String title;
    private String id;

    public Video(String t, String i) {
        title = t;
        id = i;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String toString() {
        String output = "Title: \"" + title + "\"  ID: " + id;
        return output;
    }

    @Override
    public int compareTo(Object t) {
        Video other = (Video) t;
        if (title.equals(other.getTitle()) && id.equals(other.getId())) {
            return 0;
        } else {
            return -1;
        }
    }
}

class Customer implements Comparable {

    private String name;
    private String id;
    private String dataStructure;
    public SLL rentedVideosSLL;
    public DList rentedVideosDLL;
    public BinarySearchTree rentedVideosBST;
    public AVLTree rentedVideosAVL;

    public Customer(String n, String i, String ds) {
        name = n;
        id = i;
        dataStructure = ds;
        switch (ds) {
            case "SLL":
                rentedVideosSLL = new SLL();
                break;
            case "DLL":
                rentedVideosDLL = new DList();
                break;
            case "BST":
                rentedVideosBST = new BinarySearchTree();
                break;
            case "AVL":
                rentedVideosAVL = new AVLTree();
                break;
        }
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void getRentedVideos() {
        switch (dataStructure) {
            case "SLL":
                rentedVideosSLL.print();
                break;
            case "DLL":
                rentedVideosDLL.print();
                break;
            case "BST":
                rentedVideosBST.inorderTraversal();
                break;
            case "AVL":
                rentedVideosAVL.inorderTraversal();
                break;
        }
    }

    public void setRentedVideo(Video v) {
        switch (dataStructure) {
            case "SLL":
                rentedVideosSLL.add(new Node(v, null));
                break;
            case "DLL":
                rentedVideosDLL.addLast(new DNode(v, null, null));
                break;
            case "BST":
                rentedVideosBST.insert(v);
                break;
            case "AVL":
                rentedVideosAVL.insert(v);
                break;
        }
    }

    public String toString() {
        String output = "Title: \"" + name + "\"  ID: " + id;
        return output;
    }

    @Override
    public int compareTo(Object t) {
        Customer other = (Customer) t;
        if (name.equals(other.getName()) && id.equals(other.getId())) {
            return 0;
        } else {
            return -1;
        }
    }
}

class Transaction {

    private Customer c;
    private Video v;
    private int action;

    public Transaction(Customer c, Video v, int a) {
        this.c = c;
        this.v = v;
        action = a;
    }

    public Customer getCustomer() {
        return c;
    }

    public Video getVideo() {
        return v;
    }

    public int getAction() {
        return action;
    }
}

class Node {

    private Object element;
    private Node next;

    public Node(Object e, Node n) {
        element = e;
        next = n;
    }

    public Object getElement() {
        return element;
    }

    public Node getNext() {
        return next;
    }

    public void setElement(Object e) {
        element = e;
    }

    public void setNext(Node n) {
        next = n;
    }

}

class DNode {

    private Object element;
    private DNode next;
    private DNode pre;

    public DNode(Object e, DNode n, DNode p) {
        element = e;
        next = n;
        pre = p;
    }

    public Object getElement() {
        return element;
    }

    public DNode getPre() {
        return pre;
    }

    public DNode getNext() {
        return next;
    }

    public void setElement(Object e) {
        element = e;
    }

    public void setPre(DNode p) {
        pre = p;
    }

    public void setNext(DNode n) {
        next = n;
    }

}

class SLL {

    private Node head;

    public SLL() {
        head = null;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node n) {
        head = n;
    }

    public void print() {
        if (head != null) {
            Node n = head;
            String s = "";
            while (n != null) {
                s += n.getElement() + " ";
                n = n.getNext();
            }
            System.out.println(s);
        } else {
            System.out.println("The SLL is empty");
        }
    }

    public void add(Node n) {
        if (head == null) {
            head = n;
        } else {
            Node curr = head;
            while (curr.getNext() != null) {
                curr = curr.getNext();
            }
            curr.setNext(n);
        }
        n.setNext(null);
    }

    public void remove() {
        if (head != null && head.getNext() != null) {
            Node curr = head;
            while (curr.getNext().getNext() != null) {
                curr = curr.getNext();
            }
            curr.setNext(null);
        } else if (head != null && head.getNext() == null) {
            head = null;
        }
    }

    public void reverse() {
        SLL clone = new SLL();

        Node curr = head;
        if (curr.getNext() != null) {
            while (head != null) {
                curr = head;
                while (curr.getNext() != null) {
                    curr = curr.getNext();
                }
                clone.add(curr);
                remove();
            }
        } else {
            clone.add(head);
        }

        head = clone.getHead();
    }

    public Object contains(Object element, String type) {
        Node curr = head;
        while (curr != null) {
            switch (type) {
                case "v":
                    if (curr.getElement() != null) {
                        if (((Video) curr.getElement()).compareTo(element) == 0) {
                            return curr.getElement();
                        }
                    }
                    break;
                case "c":
                    if (curr.getElement() != null) {
                        if (((Customer) curr.getElement()).compareTo(element) == 0) {
                            return curr.getElement();
                        }
                    }
                    break;
            }
            curr = curr.getNext();
        }
        return -1;
    }

    public int indexOf(Object element, String type) {
        Node curr = head;
        int i = 0;
        while (curr != null) {
            switch (type) {
                case "v":
                    if (curr.getElement() != null) {
                        if (((Video) curr.getElement()).compareTo(element) == 0) {
                            return i;
                        }
                    }
                    break;
                case "c":
                    if (curr.getElement() != null) {
                        if (((Customer) curr.getElement()).compareTo(element) == 0) {
                            return i;
                        }
                    }
                    break;
            }
            curr = curr.getNext();
            i++;
        }
        return -1;
    }

    public void insertAt(int i, Node n) {
        Node newNode = n;
        Node curr = getHead();
        if (i == 0) {
            newNode.setNext(curr);
            setHead(newNode);
        } else {
            while ((i - 1) > 0) {
                curr = curr.getNext();
                --i;
            }
            newNode.setNext(curr.getNext());
            curr.setNext(newNode);
        }
    }

    public Node removeAt(int i) {
        Node curr = getHead();
        Node temp = null;
        if (i == 0) {
            temp = curr;
            setHead(curr.getNext());
            return temp;
        } else {
            while ((i - 1) > 0) {
                curr = curr.getNext();
                --i;
            }
            temp = curr;
            if (curr != null) {
                curr.setNext(curr.getNext().getNext());
            }
        }
        return temp;
    }

    public Object[] toArray() {
        Node curr = getHead();
        if (head != null) {
            Object[] arr = new Object[size()];
            int i = 0;
            while (curr != null) {
                arr[i] = curr.getElement();
                i++;
                curr = curr.getNext();
            }
            return arr;
        }
        return null;
    }

    private int size() {
        int i = 0;
        Node curr = getHead();
        while (curr.getNext() != null) {
            i++;
            curr = curr.getNext();
        }
        return i + 1;
    }

}

class DList {

    //These are sentinel nodes, they do not hold any data
    private DNode header;
    private DNode trailer;

    public DList() {
        header = new DNode(null, null, null);
        trailer = new DNode(null, null, null);
        header.setNext(trailer);
        trailer.setPre(header);
    }

    public DNode getHeader() {
        return header;
    }

    public DNode getTrailer() {
        return trailer;
    }

    public void setHeader(DNode n) {
        header = n;
    }

    public void setTrailer(DNode n) {
        trailer = n;
    }

    public void print() {
        DNode curr = getHeader();
        if (curr.getNext() != getTrailer()) {
            while (curr.getNext() != null) {
                if (curr.getElement() != null) {
                    System.out.print(curr.getElement() + " ");
                }
                curr = curr.getNext();
            }
            System.out.println();
        } else {
            System.out.println("The DLL is empty!");
        }
    }

    public void addFirst(DNode n) {
        DNode first = header;
        DNode firstNext = header.getNext();
        first.setNext(n);
        n.setPre(first);
        n.setNext(firstNext);
        firstNext.setPre(n);
    }

    public void addLast(DNode n) {
        DNode last = trailer;
        DNode lastPre = last.getPre();
        n.setPre(lastPre);
        n.setNext(last);
        lastPre.setNext(n);
        last.setPre(n);
    }

    public DNode remove(DNode n, String type) {
        DNode curr = getHeader().getNext();
        DNode temp = null;
        boolean removeThis = false;
        int i = 0;
        while (curr.getNext() != null || removeThis == false) {
            switch (type) {
                case "v":
                    if (curr.getElement() != null) {
                        if (((Video) curr.getElement()).compareTo((Video) n.getElement()) == 0) {
                            removeThis = true;
                        }
                    } else {
                        break;
                    }
                    break;
                case "c":
                    if (curr.getElement() != null) {
                        if (((Customer) curr.getElement()).compareTo(n.getElement()) == 0) {
                            removeThis = true;
                        }
                    } else {
                        break;
                    }
                    break;
            }
            if (removeThis) {
                temp = curr;
                DNode previous = curr.getPre();
                DNode next = curr.getNext();
                previous.setNext(next);
                next.setPre(previous);
                break;
            }
            curr = curr.getNext();
            if (curr == null) {
                break;
            }
            i++;
        }
        return temp;
    }

    public void reverse() {
        DList newDll = new DList();
        if (getHeader().getNext() != getTrailer()) {
            DNode curr = getHeader().getNext();
            while (curr.getNext() != null) {
                DNode temp = new DNode(curr.getElement(), null, null);
                newDll.addFirst(temp);
                curr = curr.getNext();
            }
            header = newDll.getHeader();
            trailer = newDll.getTrailer();
        }
    }

    public Object contains(Object element, String type) {
        DNode curr = header.getNext();
        while (curr != null) {
            switch (type) {
                case "v":
                    if (curr.getElement() != null) {
                        if (((Video) curr.getElement()).compareTo(element) == 0) {
                            return curr.getElement();
                        }
                    }
                    break;
                case "c":
                    if (curr.getElement() != null) {
                        if (((Customer) curr.getElement()).compareTo(element) == 0) {
                            return curr.getElement();
                        }
                    }
                    break;
            }
            curr = curr.getNext();
        }

        return -1;
    }

    public Object[] toArray() {
        DNode curr = getHeader().getNext();
        if (curr.getElement() != null) {
            Object[] arr = new Object[size()];
            int i = 0;
            while (curr != null) {
                arr[i] = curr.getElement();
                i++;
                curr = curr.getNext();
            }
            return arr;
        }
        return null;
    }

    private int size() {
        int i = 0;
        DNode curr = getHeader().getNext();
        while (curr.getNext() != getTrailer() && curr.getNext() != null) {
            i++;
            curr = curr.getNext();
        }
        return i;
    }
}

class BinaryTreeNode implements Comparable<BinaryTreeNode> {

    public Object data;
    public BinaryTreeNode parent;
    public BinaryTreeNode left;
    public BinaryTreeNode right;

    public BinaryTreeNode(Object d) {
        data = d;
        left = null;
        right = null;
    }

    public BinaryTreeNode(Object k, Object d, BinaryTreeNode l, BinaryTreeNode r) {
        data = d;
        left = l;
        right = r;
    }

    public BinaryTreeNode(Object k, Object d, BinaryTreeNode p, BinaryTreeNode l, BinaryTreeNode r) {
        data = d;
        parent = p;
        left = l;
        right = r;
    }

    public String toString() {
        return data.toString();
    }

    public int compareTo(BinaryTreeNode target) {
        return ((Comparable) this.data).compareTo(target.data);
    }
}

abstract class BinaryTree {

    public BinaryTreeNode root;

    public void clear() {
        root = null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int size() {
        return sizeHelper(root);
    }

    private int sizeHelper(BinaryTreeNode rt) {
        if (rt == null) {
            return 0;
        }

        return sizeHelper(rt.left) + sizeHelper(rt.right) + 1;
    }

    public int height() {
        return heightHelper(root, 0);
    }

    public int height(BinaryTreeNode p) {
        return heightHelper(p, 0);
    }

    public int heightUp(BinaryTreeNode p) {
        return heightHelper2(p, 0);
    }

    private int heightHelper2(BinaryTreeNode rt, int ht) {
        if (rt == null) {
            return ht;
        }

        return heightHelper2(rt.parent, ht
                + 1);
    }

    private int heightHelper(BinaryTreeNode rt, int ht) {
        if (rt == null) {
            return ht;
        }

        return Math.max(heightHelper(rt.left, ht + 1),
                heightHelper(rt.right, ht + 1));
    }

    public String preorderTraversal() {
        System.out.print("Preorder:  ");
        preorderHelper(root);
        System.out.println();
        return "";
    }

    private void preorderHelper(BinaryTreeNode rt) {
        if (rt == null) {
            return;
        }

        System.out.print("\t" + rt.data);
        preorderHelper(rt.left);
        preorderHelper(rt.right);
    }

    public String inorderTraversal() {
        System.out.print("Inorder:  ");
        inorderHelper(root);
        System.out.println();
        return "";
    }

    private void inorderHelper(BinaryTreeNode rt) {
        if (rt == null) {
            return;
        }

        inorderHelper(rt.left);
        System.out.print("\t" + rt.data);
        inorderHelper(rt.right);
    }

    public String postorderTraversal() {
        System.out.print("Postorder:  ");
        postorderHelper(root);
        System.out.println();
        return "";
    }

    private void postorderHelper(BinaryTreeNode rt) {
        if (rt == null) {
            return;
        }

        Object[] arr = new Object[size()];

        postorderHelper(rt.left);
        postorderHelper(rt.right);
        System.out.print("\t" + rt.data);

    }

    public Object[] toArray() {
        Object[] arr = new Object[size()];
        toArrayHelper(root, arr, 0);
        return arr;
    }

    private int toArrayHelper(BinaryTreeNode rt, Object[] arr, int i) {
        if (rt == null) {
            return i;
        }

        arr[i] = rt.data;
        i++;
        if (rt.left != null) {
            toArrayHelper(rt.left, arr, i);
        }
        if (rt.right != null) {
            toArrayHelper(rt.right, arr, i);
        }

        return i;
    }
}

class BinarySearchTree extends BinaryTree {

    public Object search(Object key) {
        BinaryTreeNode curr = root;
        BinaryTreeNode node = new BinaryTreeNode(key);
        while (curr != null) {
            if (node.compareTo(curr) > 0) {
                curr = curr.left;
            } else if (node.compareTo(curr) < 0) {
                curr = curr.right;
            } else if (node.compareTo(curr) == 0) {
                return curr.data;
            }
        }
        return false;
    }

    public boolean insert(Object element) {

        BinaryTreeNode parent = null;
        BinaryTreeNode curr = root;
        BinaryTreeNode node = new BinaryTreeNode(element);

        if (isEmpty()) {
            root = new BinaryTreeNode(element);
            return true;
        } else {
            while (curr != null) {
                if (node.compareTo(curr) < 0) {
                    parent = curr;
                    curr = curr.left;
                } else if (node.compareTo(curr) > 0) {
                    parent = curr;
                    curr = curr.right;
                } else if (node.compareTo(curr) == 0) {
                    return false;
                }
            }

            if (parent.compareTo(node) > 0) {
                parent.left = node;
                node.parent = parent;
                return true;
            } else {
                parent.right = node;
                node.parent = parent;
                return true;
            }
        }
    }

    public BinaryTreeNode remove(Object element) {
        BinaryTreeNode parent = null;
        BinaryTreeNode curr = root;
        BinaryTreeNode node = new BinaryTreeNode(element);
        BinaryTreeNode temp = null;

        if (isEmpty()) {
            return temp;
        }

        while (curr != null) {
            if (node.compareTo(curr) < 0) {
                parent = curr;
                curr = curr.left;
            } else if (node.compareTo(curr) > 0) {
                parent = curr;
                curr = curr.right;
            } else if (node.compareTo(curr) == 0) {
                break;
            }
        }

        temp = curr;
        if (temp == null) {
            return temp;
        }

        if (curr.left != null && curr.right != null) {//Current node has two children, find smallest of right subtree
            BinaryTreeNode tempParent = curr;//Temp parent storage
            BinaryTreeNode tempCurr = curr.right;//The root of the right subtree
            while (tempCurr.left != null) {
                tempParent = tempCurr;
                tempCurr = tempCurr.left;
            }
            curr.data = tempCurr.data;
            tempParent.right = tempCurr.right;
            if (tempParent != root) {
                tempParent.left = null;
            }

        } else if (curr.left == null && curr.right == null) {//Current node has no children
            if (parent.compareTo(curr) > 0) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if ((curr.left != null && curr.right == null)) {//Current node has one child (left)
            parent.left = curr.left;
        } else if (curr.left == null && curr.right != null) {//Current node has one child (right)
            if (parent != null) {
                parent.right = curr.right;
            }
        }
        return temp;
    }
}

class AVLTree extends BinarySearchTree {

    @Override
    public boolean insert(Object element) {
        BinaryTreeNode parent = null;//The parent of the newly inserted node
        BinaryTreeNode current = root;//The new node being inserted
        BinaryTreeNode node = new BinaryTreeNode(element);

        if (isEmpty()) {
            root = new BinaryTreeNode(element);
            root.parent = null;
            return true;
        } else {
            while (current != null) {
                if (node.compareTo(current) < 0) {
                    parent = current;
                    current = current.left;
                } else if (node.compareTo(current) > 0) {
                    parent = current;
                    current = current.right;
                } else if (node.compareTo(current) == 0) {
                    return false;
                }
            }

            if (parent.compareTo(node) > 0) {
                parent.left = node;
                node.parent = parent;
                rebalance(node);
                return true;
            } else {
                parent.right = node;
                node.parent = parent;
                rebalance(node);
                return true;
            }
        }
    }

    @Override
    public BinaryTreeNode remove(Object element) {
        BinaryTreeNode parent = null;
        BinaryTreeNode curr = root;
        BinaryTreeNode node = new BinaryTreeNode(element);
        BinaryTreeNode temp = null;

        if (isEmpty()) {
            return temp;
        } else {
            while (curr != null) {
                if (node.compareTo(curr) < 0) {
                    parent = curr;
                    curr = curr.left;
                } else if (node.compareTo(curr) > 0) {
                    parent = curr;
                    curr = curr.right;
                } else if (node.compareTo(curr) == 0) {
                    break;
                }
            }
        }

        temp = curr;
        if (temp == null) {
            return temp;
        }

        if (curr.left != null && curr.right != null) {//Current node has two children, find smallest of right subtree
            BinaryTreeNode tempParent = curr;//Temp parent storage
            BinaryTreeNode tempCurr = curr.right;//The root of the right subtree
            while (tempCurr.left != null) {
                tempParent = tempCurr;
                tempCurr = tempCurr.left;
            }
            BinaryTreeNode t = tempCurr;
            remove(tempCurr.data);
            curr.data = t.data;
            if (tempParent != root) {
                if (tempParent.left == null && tempParent.right == null) {
                    if (tempParent != root) {
                        tempParent.right = tempCurr.right;
                    }
                }
                if (tempParent.left == null && tempParent.right == null) {
                    if (tempParent != root) {
                        tempParent.left = null;
                        rebalance(tempParent);
                    }
                }
            }

        } else if (curr.left == null && curr.right == null) {//Current node has no children
            if (parent.compareTo(curr) > 0) {
                parent.left = null;
                if (parent.left == null && parent.right == null) {
                    if (parent != root) {
                        rebalance(parent);
                    }
                }
            } else {
                parent.right = null;
                if (parent.left == null && parent.right == null) {
                    if (parent != root) {
                        rebalance(parent);
                    }
                }
            }
            curr = null;
        } else if ((curr.left != null && curr.right == null)) {//Current node has one child (left)
            parent.left = curr.left;
            if (parent.left == null && parent.right == null) {
                if (parent != root) {
                    rebalance(parent);
                }
            }
        } else if (curr.left == null && curr.right != null) {//Current node has one child (right)
            if (parent != null) {
                parent.right = curr.right;
                if (parent.left == null && parent.right == null) {
                    if (parent != root) {
                        rebalance(parent);
                    }
                }
            }
        }

        return temp;
    }

    private BinaryTreeNode sibling(BinaryTreeNode p) {
        if (p == p.parent.left) {
            return p.parent.right;
        } else {
            return p.parent.left;
        }
    }

    private boolean isBalanced(BinaryTreeNode p) {
        return (Math.abs(height(p.left) - height(p.right)) <= 1);
    }

    private void relink(BinaryTreeNode parent, BinaryTreeNode child,
            boolean makeLeftChild) {
        if (child != null) {
            child.parent = parent;
        }
        if (makeLeftChild) {
            parent.left = child;
        } else {
            parent.right = child;
        }
    }

    private BinaryTreeNode tallerChild(BinaryTreeNode p) {
        if (height(p.left) > height(p.right)) {
            return p.left;
        }
        if (height(p.left) < height(p.right)) {
            return p.right;
        }
        if (root == p) {
            return p.left;
        }
        if (p == p.parent.left) {
            return p.left;
        } else {
            return p.right;
        }
    }

    private void rotate(BinaryTreeNode p) {
        BinaryTreeNode x = p;
        BinaryTreeNode y = x.parent;
        BinaryTreeNode z = y.parent;

        if (z == null) {
            root = x;
            x.parent = null;
        } else {
            relink(z, x, y == z.left);
        }
        if (x == y.left) {
            relink(y, x.right, true);
            relink(x, y, false);
        } else {
            relink(y, x.left, false);
            relink(x, y, true);
        }
    }

    private BinaryTreeNode restructure(BinaryTreeNode p) {
        BinaryTreeNode y = p.parent;
        BinaryTreeNode z = y.parent;

        if ((p == y.right) == (y == z.right)) {
            rotate(y);
            return y;
        } else {
            rotate(p);
            rotate(p);
            return p;
        }
    }

    private void rebalance(BinaryTreeNode p) {
        do {
            if (!isBalanced(p)) {
                p = restructure(tallerChild(tallerChild(p)));
            }
            p = p.parent;
        } while (p != null);
    }
}
